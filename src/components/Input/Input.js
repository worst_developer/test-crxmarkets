import React, { Component } from 'react'
import cn from 'classnames';
import styles from './Input.module.css';

export class Input extends Component {
  render() {
    
    const { className, ...inputProps } = this.props;

    return (
      <input
        className={cn(className, styles.input)}
        { ...inputProps }
      />
    )
  }
}

export default Input

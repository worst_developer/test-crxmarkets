const api = () => {
  const get = url => fetch(
    `${process.env.REACT_APP_API_BASE_URL}${url}`, 
    {
      method: 'GET',
    }
  ).then(resp => resp.json());

  return Object.freeze({
    GET: get,
  });
}

export default api();
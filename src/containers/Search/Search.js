import React, { Component } from 'react'
import Input from 'components/Input';
import styles from './Search.module.css';

export class Search extends Component {

  handleSearch = event => {
    const value = event.target.value;

    if (value < 3) return;
    this.props.onSearch(value);
  }
  render() {
    return (
      <section className={styles.searchContainer}>
        <Input 
          placeholder='users search'
          onChange={this.handleSearch} 
        />
      </section>
    )
  }
}

export default Search;

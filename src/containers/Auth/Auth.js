import React, { Component } from 'react';
import cn from 'classnames';
import Input from 'components/Input';

import styles from './Auth.module.css';
const { REACT_APP_READONLY_SECRET , REACT_APP_CRUD_SECRET } = process.env;

const NOTIFICATION_CLOSE_TIMER = 2000; // ms

function debounce(callback, time) {
  let interval;
  return () => {
    clearTimeout(interval);
    interval = setTimeout(() => {
      interval = null;
      callback(arguments);
    }, time);
  };
}

class Auth extends Component {
  componentWillUnmount() {
    clearTimeout(this.modalTimeout);
  }

  state = {
    error: false,
    displayNotification: false,
  }

  handleValidation = (event) => {
    const value = event.target.value;

    if (!value || value.length < 3) return;

    if ([REACT_APP_READONLY_SECRET , REACT_APP_CRUD_SECRET].includes(value)) {
      this.props.onSuccess(REACT_APP_CRUD_SECRET === value, true);
      
      this.setState({
        displayNotification: true,
        error: false,
      });
    } else {
      this.setState({ 
        displayNotification: true,
        error: true 
      });
    }

    this.modalTimeout = setTimeout(() => {
      this.setState({
        displayNotification: false,
      });
    }, NOTIFICATION_CLOSE_TIMER);
  }

  handledebounced = debounce(this.handlhandleValidation, 400)

  render() {
    const { displayNotification, error } = this.state;

    return (
      <section className={styles.authContainer}>
        <Input
          className={styles.authInput}
          placeholder ='type admin-id'
          onChange={(e) => debounce(this.handleValidation(e), 400)}
        />
        {displayNotification && 
          <span className={cn(styles.authMessage, {
            [styles.success]: !error,
          })}>
          { !error && 'Success'}
          { error && 'Wrong secret'}
          </span>
        }
      </section>
    )
  };
}

export default Auth;

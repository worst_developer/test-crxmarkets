import React, { Component } from 'react';
import { connect } from 'react-redux';

import Auth from 'containers/Auth';
import Search from 'containers/Search';
import { loginRequest } from 'store/auth';
import { searchUsers, selectCountries } from 'store/search';

import styles from './App.module.css';

class Main extends Component {
  handleSelect = () => {}

  render() {
    const { loginRequest, isAuth, searchUsers, users, countries } = this.props;

    return (
      <main className={styles.mainContainer}>
        <Auth
          isAuthorized={isAuth}
          onSuccess={loginRequest}
        />
        <Search
          onSearch={searchUsers}
        />
        {users.length > 0 && 
          <table className={styles.table}>
          <tbody>
              <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Country</th>
              </tr>
            </tbody>
            <tbody>
            {users.map(({ firstname, lastname, country }) =>
              <tr key={firstname + lastname}>
                <td align="center">{firstname}</td>
                <td align="center">{lastname}</td>
                <td align="center">
                  {isAuth ? 
                    <select
                      name="countries"
                      onChange={this.handleSelect} 
                      value={country}
                    >
                      {countries.map(countryVal =>
                        <option key={firstname + countryVal} value={countryVal}>{countryVal}</option>
                      )}
                    </select> : country
                }
                </td>
              </tr>
            )}
            </tbody>
          </table>
        }
        {isAuth && <h4>🎉🎉 &nbsp;Now you are admin and can update users data&nbsp;🎉🎉</h4>}
      </main>
    );
  }
}

const mapStateToProps = state => ({
  isAuth: state.auth.isAuthorized,
  users: state.users.data,
  countries: selectCountries(state),
})

const mapDispatchToProps = {
  loginRequest,
  searchUsers,
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);

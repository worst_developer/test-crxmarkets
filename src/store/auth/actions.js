import { LOGIN_REQUEST } from './constants';

export const loginRequest = (admin, isAuth) => dispatch => {
  dispatch({
    type: LOGIN_REQUEST,
    payload: { admin, isAuth },
  })
}

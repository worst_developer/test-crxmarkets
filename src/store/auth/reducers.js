import { LOGIN_REQUEST } from './constants.js';

export const defaultState = Object.freeze({
  isAuthorized: false,
  admin: false,
});

export default (state = { ...defaultState }, action) => {
  switch (action.type) {
   case LOGIN_REQUEST: {
    const { isAuth, admin } = action.payload;
    
    return {
     isAuthorized: isAuth,
     admin,
    }
  }

   default:
    return state
  }
 }
 
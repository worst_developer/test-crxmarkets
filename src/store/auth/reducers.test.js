import reducer, { defaultState } from './reducers';
import { LOGIN_REQUEST } from './constants';

describe('auth reducer', () => {
  test('default', () => {
    const action = { type: 'SOME_NON_EXISTENT_ACTION' };

    expect(reducer(undefined, action)).toEqual(defaultState);
  });

  test(LOGIN_REQUEST, () => {
    const authData = { isAuth: true, admin: true };

    const action = {
      type: LOGIN_REQUEST,
      payload: { ...authData },
    };

    const expected = {
      ...defaultState,
      isAuthorized: authData.isAuth,
      admin: authData.admin,
    };

    expect(reducer(defaultState, action)).toEqual(expected);
  });
});
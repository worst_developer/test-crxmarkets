import { combineReducers } from 'redux';
import authReducer from 'store/auth';
import usersReducer from 'store/search';

export default combineReducers({
  auth: authReducer,
  users: usersReducer,
});

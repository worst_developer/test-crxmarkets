import { selectCountries } from './selectors';

describe('select countries', () => {
  test('should filter dubplicates in array of objects', () => {
    const state = {
      users: {
        data: [
          {
            name: 'User',
            country: 'de'
          },
          {
            name: 'User1',
            country: 'us'
          },
          {
            name: 'Ipsum',
            country: 'pl'
          },
          {
            name: '12343',
            country: "ua"
          },
          {
            name: 'lorem',
            country: "ua"
          } 
          ]
      }
    };

    const expected = ['de', 'us', 'pl', 'ua'];

    expect(selectCountries(state)).toEqual(expected);
  });
});

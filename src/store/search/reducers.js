import { SEARCH_USERS_SUCCESS, SEARCH_USERS_REQUEST, SEARCH_USERS_FAILURE } from './constants';

const defaultState = Object.freeze({
  data: [],
});

export default (state = { ...defaultState }, action) => {
  switch (action.type) {
   case SEARCH_USERS_REQUEST:
     return {
        data: [],
        isLoading: true
      }
   case SEARCH_USERS_SUCCESS: {
    // data is hardcoded because of broken api

    const data = {
      users : [
          {
              lastname: "Muller",
              firstname: "Graham",
              country: "de"
          },
          {
              lastname: "Murgh",
              firstname: "Ellis",
              country: "ru"
          },
          {
              lastname: "Muesli",
              firstname: "Peterrson",
              country: "ru"
          },
          {
              lastname: "Murink",
              firstname: "Edward",
              country: "de"
          },
          {
              lastname: "Musuf",
              firstname: "Arnold",
              country: "de"
          },
          {
              lastname: "Santori",
              firstname: "Oliver",
              country: "ru"
          },
          {
              lastname: "Zuzi",
              firstname: "Elvis",
              country: "de"
          }
          ]
    };

    return {
     data: data.users,
     isLoading: false,
    }
  }

  case SEARCH_USERS_FAILURE: {
    const { error } = action.payload;

    return {
      isloading: false,
      error: error || 'Something went wrong'
    }
  }

   default:
    return state
  }
 }

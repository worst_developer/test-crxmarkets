import {
  searchUsersRequest,
  searchUsersSuccess,
  searchUsersFailure,
} from './actions';
// import { getUsers } from 'modules/api';


export const searchUsers = search => async dispatch => {
  try {
    dispatch(searchUsersRequest(search));
    
    /*
      api response is broken and it is not possible to use it so I'am going to hardcode in success reducer
      
      const users = await getUsers();
      dispatch(searchUsersSuccess(users));
    */
    dispatch(searchUsersSuccess());
  } catch (error) {
    console.log(error)
    dispatch(searchUsersFailure(error));
  }
}

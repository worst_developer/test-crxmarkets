import { SEARCH_USERS_SUCCESS, SEARCH_USERS_REQUEST, SEARCH_USERS_FAILURE } from './constants';

export const searchUsersRequest = search => ({
  type: SEARCH_USERS_REQUEST,
  payload: { search },
});

export const searchUsersSuccess = data => ({
  type: SEARCH_USERS_SUCCESS,
  payload: { data },
});

export const searchUsersFailure = error => ({
  type: SEARCH_USERS_FAILURE,
  payload: { error },
});


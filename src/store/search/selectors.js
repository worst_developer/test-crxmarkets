export const selectCountries = state => {
  return [...new Set(state.users.data.map(user => user.country))];
}
import React from 'react';

export const { Provider, Consumer } = React.createContext({ 
  isAuthorized: false,
  handleAuth: () => {},
});
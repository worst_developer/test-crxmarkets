This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


# Info from developer

✋ Hi, here it is list of what was done:
 - created scalable architecture
 - separated dumb and smart components
 - written api module
 - added and configured redux to the project as a store manager(will explain later why it is a big pain :) )
 - added:
    - authentication
    - simple UI
    - success message
    - search of users
    - display users data
    - possibility to change user country depending on admin rights

And now long text from me😬. I used CRA because it is the fastest way to rump-up small project or tech task not always the best choice for something big because of it is hard configuration. Personally I like to use mobX for such projects but this time everything went wrong (maybe karma) mobX is fast and easy in configuration (it was) but this time I had big problems to configure it because was not using for long time and I had loosed a lot of time to run it and I loosed, so because of leak of time I removed mobx and then tried run with ReactContext but it without rude words it was bad choice (I do not like React.Context for read and write for read-only it is pretty good but for updating parent components it is a bad choce because it is not scalable solution) I started to write with context but I see that it would not work as a scalable solution it will be 💩 with implemented requirements but not Senior level structure. So I rushed to install Redux (you can ask why I did not run with Redux from start because I do not like big pattern code of redux espacially in a such small project) with Redux proccess started to grow very fast bam actions, bam api module, bam connected store to the project, well-structured architecture in time that I had. I had problems with api since it is broken to use it I had hardcoded it is response it search reducer but everything is ready to use real route. About project structure itself I prefer to keep things simple so dumb Components are in [components](./src/components) folder, Smart in [containers](.src/containers), [store](./src/store) folder for reudx-related stuff dived by it is functionality or modules as you wish, also I like to keep tests near file that should be tested to make it more visible instead of long searching through project also you can find [modules](.src/modules) I am using it for additional services or logic that are not good place in thunks like storing data in local-storage, hashing data and stuff like that, also I like to add utils folder for array, object, calculations and other helper functions. I do not what else to add I would like to build one more enterprise project from scratch. Hopefully you will get all that mess of minds that I wrote to you🙃. Have a nice day and will be happy to have at least technical call with you.